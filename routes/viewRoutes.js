const express = require('express')
const router = express.Router()
const viewController = require('../controller/viewController')



router.get('/', viewController.getHome)
router.get('/protfolioDetails', viewController.getProtfolioDetails)
router.get('/protfolioDetails1', viewController.getProtfolioDetails1)

module.exports = router