const express = require("express")
const app = express()
const path = require('path')
const viewRoutes = require('./routes/viewRoutes')



app.use('/', viewRoutes)
app.use(express.static(path.join(__dirname, 'views')))



const port = 4001
app.listen(port, () => {
    console.log('App running on port 4001...')
})