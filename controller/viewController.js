const path = require('path')

exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'index.html'))
};
exports.getProtfolioDetails = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'portfolio-details.html'))
};
exports.getProtfolioDetails1 = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'portfolio-details1.html'))
};

